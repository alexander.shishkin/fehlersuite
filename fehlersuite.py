################################################################# Todo
'''
- linreg berechnungen nochmal überprüfen
- mir erklären was ensure_nparray macht in der logik mit den tupeln (glaube ich) 
- ensure_nparray reparieren, sodass sie in gauss_fort funktioniert
- die fehlersuite als modul über pip installiere
- addlin vernünftig machen
- auf gitlab das packen
- draw_coords integrieren
'''

##################changelog
'''
- 8.7.2023: add_lin auf listen erweitert, ebenso sub_lin zugefügt
'''

#tikzplotlibinstaller (da tikzplotlib shieße ist brauchen iwr das nicht)
#import subprocess
#import sys
#subprocess.check_call([sys.executable, "-m", "pip", "install", "tikzplotlib"])
#import tikzplotlib as tpl # tplsave(). die library muss gefühlt jedes mal neu instaliert werden, einfach code oben verwenden.


import sympy as sp # für gauss_fort
import numpy as np # für, ka, alles?
from scipy import stats, optimize # für student t faktor in bestwert68
import matplotlib.pyplot as plt # ist nützlich und für tikzplotlib
import itertools #braucht man immer (insbesondere add_lin)

################################################################# Konstanten
CtK = 273.15
g = (9.81, 0)

################################################################# Helfer
def is_sammlung(val):
    return type(val) == tuple or type(val) == list or type(val) == np.ndarray

def is_iterable(x): #anscheinend gibt es literally keine brauchbare funktion dafür 
    try:
        iterator = iter(x)
        return True
    except TypeError as te:
        return False
def is_list(x):
    return type(x) == list or type(x) == np.ndarray

def ensure_nparray(array): # ich glaube einfach mal das das richtig funktioniert
    #array = np.array(array) if type(array) == list else array
    #return array
    if is_iterable(array):
        np_array = []
        for element in array:
            if is_iterable(element) and type(element) is not np.ndarray and type(element) is not tuple:
                np_array.append(ensure_nparray(element))
            elif type(element) is tuple:
                tup = []
                for value in element:
                    tup.append(ensure_nparray(value))
                np_array.append(tuple(t for t in tup))
            else:
                np_array.append(element)
        return np.array(np_array, dtype=object) # DAS DTUYPE=OBJECT IST SOZIEMLICH GERATEN OB DAS RICHTIG IST
    else:
        return array

################################################################# Formattierer
def format_val_err(val, err, ausklammern=True): #setzt es in der schreibwiese wie siunitx es haben will
    err = np.abs(err)
    #if np.abs(val) < err:
        #print("Achtung! komplett unbedachter code in format_val_err wird ausgeführt (gigantischer fehler)")
        #raise ValueError("Der Fehler des Fehlers ist gigantisch")
    
    
    #nach din1333-norm runden
    din_potenz = int(np.floor(np.log10(err))) #wir verschieben um din_potenz, sodass beim Fehler nur noch eine Stelle vor dem Komma ist, und truncaten dann (und verschieben wieder zurück)
    err = err * (10.**(-din_potenz));
    if (err < 3):
         err = err * 10; din_potenz -= 1
        
    err = np.round(np.ceil(err) * 10.**(din_potenz), -din_potenz) #np.round um cursed floating point error zu beseitigen
    val = np.round(np.round(val * 10.**(-din_potenz)) * 10.**(din_potenz), -din_potenz) #val wird nicht truncated sondern gerundet!
    
    if ausklammern == False:
        return f"{val}({err})"
    
    
    #in e-notation für LaTeX umwandeln
    aus_potenz = int(np.floor(np.log10(np.abs(val)))) #zum ausklammern verschieben wir um aus_potenz
    nach_stellen = din_potenz - aus_potenz #verschieben zum truncaten an din_potenz, dann ausklammern bis aus_potenz, also ist die höchste verbleibende Stelle din_potenz - aus_potenz
    
    #durch das round im return können wir die cursed flaoting point fehler eliminieren
    val = np.round(val * 10.**(-aus_potenz), -nach_stellen)
    err = np.round(err * 10.**(-aus_potenz), -nach_stellen)
    
    if (nach_stellen < 0):
        precision = "." + str(np.abs(nach_stellen)) + "f"
    elif (nach_stellen > 0):
        precision = str(nach_stellen) + "f"
    else:
        #raise ValueError("Ich weiß nicht wie die korrekte Formatierung in diesem Fall ist und überlasse es hiermit der ersten Person, der es passiert, das rauszufinden")
        #############################################################print("achtung! komplett geratener code in format_val_err wird ausgeführt!")
        precision = ".0f"
        
    return format(val, precision) + "(" + format(err, precision) + ")e" + str(aus_potenz)

def latex_val(_val, _err=None, doprint=None): #Die reihenfolge von assignment und fehlerprüfüng ist bewusst ineffizient gewählt
    if (is_sammlung(_val)):
        val = _val[0]
        err = _val[1]
        if not _err is None:
            raise ValueError("Nicht mehr als ein Wertepaar in latex_val!")
        if len(_val) > 2:
            raise ValueError("Zu viele Werte(paare) eingegeben")
    else:
        val = _val
        err = _err
        if _err == None:
            raise ValueError("Kein None als Fehler!")
            #Ka ob man das wirklich als fehler haben will
        if is_sammlung(_err):
            raise ValueError("Mehr als ein Fehlerwert")
        
    latex = f"\\SI{{{format_val_err(val, err)}}}"#fixed point notation
    if doprint == None:
        print(latex) 
    else:
        return latex

def latex_tab(vals, errs=None, doprint=None, din1333=None, sicolumn=True, ausklammer=None, enumerate_zeilen=False):
    #Aufbau: vals ist liste von spalten. errs auch, aber eine leere liste oder none ist "ohne fehler". ausklammer erlaubt einem aus Spalten einen faktor auszuklammern
    #sicolumn ignoriert ausklammer (da man es in latex machen kann mit drop exponent)
    if ausklammer is None:
        ausklammer = len(vals)*[None]
    elif not is_list(ausklammer):
        ausklammer = len(vals)*[ausklammer]
    elif len(ausklammer) != len(vals):
        raise ValueError("ausklammer und vals haben nicht gleichviele werte")
        
    #ob man nun die val_err mit din1333 formatiert haben will
    if din1333 is None:
        din1333 = len(vals)*[True]
        
    #ob man nun die die spalte in latex mit S formatieren will
    if sicolumn is None:
        sicolumn = len(vals)*[False]
        
    if not is_sammlung(sicolumn):
        sicolumn = len(vals)*[sicolumn]
        
    if errs is None: #vals ist liste von tupeln und muss erst entpackt werden (oder auch nicht)
        if is_iterable(vals):
            if is_sammlung(vals[0]) and len(vals[0]) > 2:
                raise ValueError("Zu viele Einträge im verkürzen Aufschreiben (nur Wert plus Fehler ist möglich)")
            if is_sammlung(vals[0]) and len(vals[0]) == 2:
                errs = [val[1] for val in vals]
                vals = [val[0] for val in vals]
            elif is_sammlung(vals[0]) and len(vals[0]) == 1:
                vals = [val[0] for val in vals]
                errs = [None]
            else:
                errs = [None]
        else:
            raise ValueError("bruh es ist ein wert den du versuchst einen wert in eine tabelle zu packen")
            
    if not is_sammlung(vals[0]):
        #print("interpretiere ein einsames array als array von array")
        vals = [vals]
        errs = [errs]
                
                
    if len(vals) != len(errs):
        # Habe mich entschieden das das doch kein Fehler ist
        #raise ValueError("nicht gleich viele Fehlerspalten wie Wertespalten")
        print("Warning: latex_tab hat nicht gleiche Dimensionen für Fehler und Werte. Wird nun aufgefüllt")
            
    # Auffüllen, falls es nicht ganz passt:
    if len(vals) < len(errs):
        errs = errs[:len(vals)]
    if len(errs) < len(vals):
        errs.append([None]*(len(vals)-len(errs)))# hier sollte es kein problem sein, dass die leeren arrays eigentlich alle gleich sind, da diese nicht verwendet werden (python ""magie"")
    
    tab_len = len(vals[0])
    for n, l in enumerate(vals):
        if not len(l) == tab_len:
            raise ValueError(f"Spalte {n} in vals hat eine andere Länge als Spalte 0")

    for n, l in enumerate(errs):
        if l is not None:
            if not is_list(l):
                errs[n] = tab_len*[l]
            elif not len(l) == tab_len:
                raise ValueError(f"Spalte {n} in vals hat eine andere Länge als Spalte 0")
    
    #es sollte nun so formatiert sein wie es sein sollte
    
    
    table = [] #für le multiline: https://www.askpython.com/python/examples/add-a-newline-character-in-python
    
    
    for y in range(tab_len):
        row = ""
        if enumerate_zeilen:
            row += str(y+1) + " & "
        for x in range(len(vals)):
            if not sicolumn[x]:
                if ausklammer[x] is None:
                    row += str(vals[x][y]) if (errs[x] is None or len(errs[x]) == 0) else f"(\\num{{{format_val_err(vals[x][y], errs[x][y], ausklammern=din1333[x])}}})"
                    #row += str(vals[x][y]) if (errs[x] is None or len(errs[x]) == 0) else f"(\\num{{{vals[x][y]}({errs[x][y]})}})"
                else:
                    row += str(vals[x][y]/ausklammer[x]) if (errs[x] is None or len(errs[x]) == 0) else f"(\\num{{{format_val_err(vals[x][y]/ausklammer[x], errs[x][y]/ausklammer[x], ausklammern=din1333[x])}}})"
            else:
                row += str(vals[x][y]) if (errs[x] is None or len(errs[x]) == 0) else format_val_err(vals[x][y], errs[x][y], ausklammern=din1333[x])
            row += " & " if x + 1 < len(vals) else "\\\\"
        table.append(row)
        
    table_str = "\n".join(table)
    if doprint == None:
        print(table_str)
    else:
        return table_str
    
def din1333(_val, _err=None): #rundet nach: https://www.ptb.de/cms/fileadmin/internet/fachabteilungen/abteilung_1/1.2_festkoerpermechanik/1.22/276._PTB-Seminar.Teil_4.Rundungsregeln.pdf (habe auch erfolgreich archiviert in wayback
    print("din1333 wird depreciated. alle printer greifen zurück auf format_val_err, was jetzt diesee funktionalität übernimmt")
    z = None #(siehe unten)
    
    if (type(_val) == np.ndarray or type(_val) == list) and (type(_err) == np.ndarray or type(_err) == list):
        n = len(_val)
        if n != len(_err):
            raise ValueError("Werte und Fehler haben nicht gleiche Dimension")
        
        val = []
        err = [] 
        for i in range(n):
            v, e = din1333(_val[i], _err[i])
            val.append(v)
            err.append(e)
    #falls man nur einen error wert übergeben hat
    elif (type(_val) == np.ndarray or type(_val) == list) and _err is not None: #und errval ist folglich kein array (insbesondere hoffentlich also eine zahl) weil sonst wäre ja das darüber getriggered
        n = len(_val)
        val = []
        err = _err
        for i in range(n):
            v, _ = din1333(_val[i], _err)
            val.append(val)
    elif (type(_val) == np.ndarray or type(_val) == list) and _err == None: # Es wird versucht _val als 2 datensätze zu interpretieren
        n = len(_val)
        val = []
        err = []
        for i in range(n):
            v, e = din1333(_val[i][0], _val[i][1])
            val.append(v)
            err.append(e)
    elif type(_val) == tuple:# hoffentlicht ruft niemals jemals wer das auf mit den werten in tupeln anstatt listen (länge tupel = n)
        v, e, *z = _val #wenn _val ein tuple ist, dann wurde vermutlich din1333(foo()) gemacht, in dem Fall wollen wir, falls foo() mehr als zwei rückgabewerte hat, die ersten beiden runden und die restlichen in form eines arrays weitergeben; https://www.geeksforgeeks.org/unpacking-a-tuple-in-python/
        val, err = din1333(v, e)
    elif _err == 0:
        print("Warning: der Fehler in din1333 ist gleich 0. Es wird die Eingabe ausgegeben")
        val = _val; err = _err
    elif _err == np.Inf or _err == float("inf"):
        raise ValueError("Fehler ist literally unendlich")
    else: # Algorithmus wurde unerändert gelassen.
        val = _val; err = _err
        potenzfaktor = 1

        while (err >= 10):
            err = err / 10; potenzfaktor = potenzfaktor / 10
        while (err < 1):
            err = err * 10; potenzfaktor = potenzfaktor * 10
        if (err < 3):
            err = err * 10; potenzfaktor = potenzfaktor * 10
    
        err = int(err); err += 1; err = err / potenzfaktor
        val *= potenzfaktor; val = np.round(val); val = val / potenzfaktor
    
    if z == None or z == []:
        return val, err
    else:
        return val, err, *z #siehe elif type(_val) == tuple: ;

################################################################# Fehlerrechnung
def add_gauss(val1, val2):
    val = val1[0] + val2[0]
    err = np.sqrt(val1[1] ** 2  + val2[1] ** 2)
    return (val, err)

def mitte_gauss(val1, val2):
    v, e = add_gauss(val1, val2)
    return (0.5*v, 0.5*e)

def mitte_lin(val1, val2):
    v, e = add_lin(val1, val2)
    return (0.5*v, 0.5*e)

def add_lin(val1, val2): #hier ist vieles unvollständig
    if is_sammlung(val1[0]) or is_sammlung(val2[0]):
        if is_sammlung(val1[0]) and is_sammlung(val2[0]):
            if len(val1[0]) != len(val2[0]):
                raise ValueError("die eingaben sind nicht gleichlang")
        #if len(val1[1]) != len(val2[1]): #todo: hier muss man zuerst prüfen dass es sammlungen sind
        #    raise ValueError("die fehler sind nicht gleichlang")
        
        val1 = list(val1)
        val2 = list(val2)
        #inlistenpackung values
        if not is_sammlung(val1[0]):
            val1[0] = [val1[0]]
        if not is_sammlung(val2[0]):
            val2[0] = [val2[0]]
            
        #inlistenpackung fehler
        if not is_sammlung(val1[1]):
            val1[1] = len(val1[0])*[val1[1]]
        if not is_sammlung(val2[1]):
            val2[1] = len(val2[0])*[val2[1]]
            
        #hier machen wir was lustiges gleich, https://stackoverflow.com/questions/19686533/how-to-zip-two-differently-sized-lists-repeating-the-shorter-list
        zip_list_0 = zip(val1[0], itertools.cycle(val2[0])) if len(val1[0]) > len(val2[0]) else zip(itertools.cycle(val1[0]), val2[0])
        zip_list_1 = zip(val1[1], itertools.cycle(val2[1])) if len(val1[1]) > len(val2[1]) else zip(itertools.cycle(val1[1]), val2[1])

        return (np.array([v1+v2 for v1, v2 in zip_list_0]), np.array([v1+v2 for v1, v2 in zip_list_1]))
    
    else:
        return (val1[0] + val2[0], val1[1] + val2[1])
    
def sub_lin(val1, val2): #val1 - val2
    return add_lin(val1, neg(val2))


def add_systematic(val, syserr):
    return (val[0], val[1] + syserr)

def neg(tup):
    return (-1*tup[0], tup[1])

def gewichtetes_mittel(vals, errs=None): #I.9 in Knetter
    #entficken, falls die typen nicht so sind wie sein sollten (diese paar zeilen sind zeimilch cursed)
    if is_sammlung(vals) and errs is None: #ich werde nicht lügen, mache sachen hier sind ziemlich dumm
        if len(vals) == 0: # ich schwöre wenn hier jemald eine leere liste als argument landet
            raise ValueError("Es gibt keine Werte")
        elif len(vals) == 1:
            print("Waring: Es wurden keine Fehler angegeben. Es wird das Avg ohne Fehler zurückgegeben")
            return np.average(vals), 0
        elif is_sammlung(vals[0]):# evtl kann das was retten
            print("werning: es wird versucht ganz cursed in gewichtetes_mittel aus einer eingabeliste values und errors zu finden")
            errs = vals[1]
            vals = vals[0]
        #TODO: hier könenn leicht cursd sachen aufkommen
        
    if not is_sammlung(errs):
        errs = len(vals)*[errs]

    val_numerator, val_denominator, err_numerator, err_denominator = 0, 0, 0, 0
    for i in range(len(vals)):
        val_numerator += vals[i] / (errs[i] ** 2)
        val_denominator += 1 / (errs[i] ** 2)
    val = val_numerator / val_denominator
    for i in range(len(vals)):
        err_numerator += (vals[i] - val) ** 2 / (errs[i] ** 2)
        err_denominator += 1 / (errs[i] ** 2)
    err = np.sqrt(err_numerator / ((len(vals) - 1) * err_denominator))
    
    return val, err

#falls man ein array von arays hat wo man gewichtetes mittel machen will, aber immer jeweils den i-ten eintrag des arrays
def gew_mittel_horizontal(vals, errs):
    return tuple(np.transpose([np.array(gewichtetes_mittel(vs, es)) for vs,es in zip(np.transpose(vals), np.transpose(errs))]))

def bestwert68(vals, occurences=None): #bestwert mit 1-sigma (68.3%) (für statistische abweichung » systematische abweichung) (will man noch systematische abweichung draufklatschen, kann man ja ein add_lin noch machen)
    vals = np.ndarray(vals) if (type(vals) == list) else vals
    if occurences is None:
        occurences = [1]*len(vals)
    
    count = np.sum(occurences)
    mean = np.dot(vals, occurences) / count
    var = np.dot(np.square(vals - mean), occurences) / (count - 1)
    std = np.sqrt(var)
    
    #student-t faktor (siehe knetter)
    #https://stackoverflow.com/questions/19339305/python-function-to-get-the-t-statistic
    t = stats.t.ppf(0.5 + 0.683/2, count-1)#das sollte so richtig sein, zumindest für n=3,5
    err = std * t * (1 / np.sqrt(count)) 
    
    return mean, err
    
def gauss_fort(expr, symbols, vals_errs, debug=False):
    if not is_list(vals_errs):
        vals_errs = [vals_errs]
        
    vals = [val_err[0] for val_err in vals_errs]
    vals = vals[0] if len(vals) == 1 else vals #1-tupel eliminieren
    gauss_expr = 0 #das wird zum term unter der wurzel
    gauss_expr_latex = ""
    all_syms = []
    
    
    if type(symbols) is tuple:
        for i, s in enumerate(symbols):
            errsym = sp.symbols(r"\sigma_{" + str(s) + "}")
            err_i = sp.diff(expr, s) * errsym
            gauss_expr = gauss_expr + (err_i) ** 2
            gauss_expr_latex += "\\left(" + sp.latex(err_i) + "\\right)^2" # vielleicht geht das vielleicht auch nicht
            gauss_expr_latex += " + " if i < len(symbols) - 1 else ""
            all_syms.append((s, errsym))
        gauss_expr = sp.sqrt(gauss_expr) #muss nur die Wurzel ziehen falls es mehr als einen Ausdruck gibt, sonst abs
        gauss_expr_latex = "\\sqrt{" + gauss_expr_latex + "}"
    else:
        errsym = sp.symbols(r"\Delta{" + str(symbols) + "}")
        gauss_expr = sp.Abs(sp.diff(expr, symbols) * errsym) #nur ein term -> abs betrag nehmen
        gauss_expr_latex = sp.latex(gauss_expr)
        all_syms.append((symbols, errsym))
    
    lam_gauss = sp.lambdify([all_syms], gauss_expr, "numpy") #https://stackoverflow.com/questions/59632912/lambdify-returns-typeerror-lambdifygenerated-missing-1-required-positional-a
    lam_expr = sp.lambdify([symbols], expr, "numpy")
    
    if debug == True:
        print("gauss", tuple(vals_errs))
    
    return lam_expr(ensure_nparray(vals)), lam_gauss(tuple(vals_errs)), gauss_expr_latex #beim zweiten argument war ein ensure_nparay, das habe ich weggemacht
      
################################################################# Appendix
global __appendix_err
__appendix_err = [[]]
def reset_appendix():
    __appendix_err = [[]]

reset_appendix()
    
def appendix_add_err(eq_latex, name, symbol, label="eq:fehler:"):
    label += str(len(__appendix_err))
    __appendix_err.append(["Der Fehler für " + name + " $" + symbol + "$ ergibt sich durch",
          "\\begin{equation}\\label{" + label + "}",
          "\t\\Delta " + symbol + " = " + eq_latex + ".", 
          "\\end{equation}",
          ""])
    
def print_appendix():
    print("\subsection{Fehlerformeln}\label{sec:fehler}")
    for eq in __appendix_err:
        for line in eq:
            print(line)

################################################################# Misc.
def save_csv(filename, cols, header="", fmt="%f"): #https://numpy.org/doc/stable/reference/generated/numpy.savetxt.html
    np.savetxt(filename, np.transpose(np.array(cols)), delimiter=",", header=header, fmt=fmt, comments="") #https://www.statology.org/numpy-array-to-csv/
    
def tikzplotlib_fix_ncols(obj):
    """
    workaround for matplotlib 3.6 renamed legend's _ncol to _ncols, which breaks tikzplotlib
    """
    if hasattr(obj, "_ncols"):
        obj._ncol = obj._ncols
    for child in obj.get_children():
        tikzplotlib_fix_ncols(child)

################################################################# Regression
def linear(x, m, b):
    return m * x + b

def linreg_internal2(func):
    # Perform the curve fitting
    initial_guess = [1, 1, 1]  # Initial parameter guess
    params, params_covariance = curve_fit(func, x, y, p0=[0,0])
    m, b = params

    # Calculate the standard deviations of the parameters
    param_errors = np.sqrt(np.diag(params_covariance))
    # Define the confidence level (e.g., 1 sigma corresponds to 68.3% confidence)
    confidence_level = 1.0
    param_intervals = param_errors * confidence_level

    return m, b, param_intervals[0], param_intervals[1]

# so was ähnliches kann man auch mit anderen funktionenarten machen
#https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
#https://faculty1.coloradocollege.edu/~sburns/LinearFitting/SimpleDataFittingWithError.html
def linreg(x_vals, y_vals, y_errs=None):
    x_vals = ensure_nparray(x_vals)
    y_vals = ensure_nparray(y_vals)

    
    if is_iterable(y_vals[0]) and is_iterable(x_vals[0]):
        m = []; m_err = []; b = []; b_err = []
        #try:
        for n, (x_reihe, y_reihe) in enumerate(zip(x_vals, y_vals, strict=True), start=1):
            if len(x_reihe) != len(y_reihe):
                print(x_reihe, y_reihe)
                raise ValueError(f"Es gibt nicht gleich viele Einträge in Messreihe {n} für x und y")
            if y_errs is None:
                y_err = None
            elif not is_list(y_errs):
                y_err = y_errs
            else:
                y_err = y_errs[n-1]#da start = 1
                    
            _m, _m_err, _b, _b_err = linreg(x_reihe, y_reihe, y_errs=y_err)# ich hoffe keiner benuzt das jemals für listen von listen von messreihen… sollte aber ohne probleme funktionieren
            m.append(_m); m_err.append(_m_err); b.append(_b); b_err.append(_b_err)
        #except ValueError as e:
        #    print(e)
        #    raise ValueError(f"Es gibt nicht gleich viele x- und y-Messreihen ({len(x_vals)} vs {len(y_vals)})") from e# baba übersichtlich (warum zum fick wird der eine error gecatcht? ich galube das sollte nur auf das zip(..., strict=True) beziehen)
            
    elif is_iterable(y_vals[0]) and not is_iterable(x_vals[0]): #warum würde jemand so etwas jemals tun? (antwort: man ist 3-sigma chad)
        m = []; m_err = []; b = []; b_err = []
        
        for n, y_reihe in enumerate(y_vals, start=1):
            if len(x_vals) != len(y_reihe):
                raise ValueError(f"Es gibt nicht gleich viele Einträge in Messreihe {n} für x und y")
            _m, _m_err, _b, _b_err = linreg(x_reihe, y_reihe)
            m.append(_m); m_err.append(_m_err); b.append(_b); b_err.append(_b_err)
    else:
        
        if y_errs is not None:
            if not is_iterable(y_errs):# falls man nur eine zahl als input macht
                y_errs = [y_errs]*len(y_vals)
            y_errs = ensure_nparray(y_errs)
            y_errs[y_errs == 0] = y_errs[y_errs>0].min()
            
            if len(x_vals) != len(y_vals) or len(y_vals) != len(y_errs):
                #print(x_vals, y_vals, y_errs)
                raise ValueError("Die Längen die Einträge sind nicht gleich")
        
            #formel ist so wie in knetter: es werden zuerst die usmmen ausgerechent. die name der variablen sind wovon die summe ist
            ss = np.sum(1/y_errs ** 2)
            xxss = np.sum(np.divide(x_vals, y_errs) ** 2)
            xss = np.sum(np.divide(np.divide(x_vals, y_errs), y_errs))
            yss = np.sum(np.divide(np.divide(y_vals, y_errs), y_errs))
            xyss = np.sum(np.divide(np.multiply(x_vals, y_vals), y_errs**2))
        
            Delta = ss*xxss - (xss**2)
            m = 1/Delta * (ss*xyss - xss*yss)
            b = 1/Delta * (xxss*yss - xss*xyss)
            chi_sq = np.sum(np.divide(np.subtract(y_vals, m * x_vals) - b, y_errs)**2)
            m_err = np.sqrt(ss/Delta)
            b_err = np.sqrt(xxss/Delta)
        else:
            if len(x_vals) != len(y_vals):
                raise ValueError("Die Längen die Messreihen sind nicht gleich")
            
            if len(x_vals) < 3:
                raise ValueError("Es sind zu wenig Daten da um sinnvoll Regression zu machen (Bei nur 2 kann man keine Fehlerangabe machen)")
            
            #bezeichnung wie oben
            
            x = np.sum(x_vals)
            y = np.sum(y_vals)
            xy = np.sum(np.multiply(x_vals, y_vals))
            xx = np.sum(x_vals ** 2)
            
            n = len(x_vals)
            m = (n*xy - x*y)/(n*xx - x**2)
            b = (xx*y-x*xy)/(n*xx - x**2)
            
            errsq = np.sum(np.subtract(y_vals, m * x_vals) - b)**2
            
            m_err = n*errsq/(n-2)/(n*xx-x**2)
            b_err = xx*errsq/(n-2)/(n*xx-x**2)
    if is_list(m):
        m = np.array(m)
        b = np.array(b)
        m_err = np.array(m_err)
        b_err = np.array(b_err)
    return m, m_err, b, b_err

################################################################# Printer
def pr(titel, tup):
    print(str(titel))
    latex_val(tup[0:2])
    if len(tup) > 2:
        print(tup[2] + "\n")  
        return tup[2:]
    return
    
def p(titel, formel, symbole, daten):
    return pr(titel, gauss_fort(formel, symbole, daten))

#für pgfplots.
def print_coords(x_arr, y_arr, dx=None, dy=None, do_print=True):
    s = ""
    if not is_list(x_arr) or not is_list(y_arr):
        raise ValueError("x Array oder y Array ist keine Liste")
        
    if len(x_arr) != len(y_arr):
        raise ValueError("x- und y- Array sind nicht gleich lang")
        
    #wahl is_list und is_iter ist arbiträr hier glaube ich
    if dy is not None and not is_iterable(dy):
        dy = [dy] * len(y_arr)
    if dx is not None and not is_iterable(dx):
        dx = [dx] * len(x_arr)
        
        
    if dx is not None and len(dx) != len(x_arr):
        raise ValueError("Anzahl x_fehler ist ungleich x Werte")
    if dy is not None and len(y_arr) != len(dy):
        raise ValueError("Anzahl y_fehler ist ungleich y Werte")

    if dx is None and dy is None:
        for x, y in zip(x_arr,y_arr):
            s += f"({x}, {y})\n"
    elif dx is not None and dy is None:#vielleicht ist hier die ausgabe nicht pgfplotskonform
        for x,dx,y in zip(x_arr,dx,y_arr):
            s += f"({x}, {y}) +- ({dx})\n"
    elif dy is not None and dx is None:#vielleicht ist hier die ausgabe nicht pgfplotskonform
        for x,dy,y in zip(x_arr,dy,y_arr):
            s += f"({x}, {y}) +- ({dy})\n"
    else:
        for x,dx,y,dy in zip(x_arr,dx,y_arr,dy):
            s += f"({x}, {y}) +- ({dx}, {dy})\n"
    if do_print:
        print(s)
    else:
        return s

def print_errorband(x_arr, y_arr, err_arr, do_print=True):
    if not is_list(x_arr) or not is_list(y_arr):
        raise ValueError("x Array oder y Array ist keine Liste")
        
    if len(x_arr) != len(y_arr):
        raise ValueError("x- und y- Array sind nicht gleich lang")
        
    #wahl is_list und is_iter ist arbiträr hier glaube ich
    if err_arr is not None and not is_iterable(err_arr):
        err_arr = [err_arr] * len(y_arr)
    elif err_arr is None:
        raise ValueError("Kein Fehler angegeben")
        
    if err_arr is not None and len(y_arr) != len(err_arr):
        raise ValueError("Anzahl y_fehler ist ungleich y Werte")

    s = "\\begin{filecontents*}{data.dat}\n" #eventuell könnte es problematisch sein dass es immer den gleichen Namen hat
    s += f"x\t y\t err \n"
    for x, y, err in zip(x_arr,y_arr, err_arr):
        s += f"{x}\t {y}\t {err} \n"
    s += "\end{filecontents*}"
    if do_print:
        print(s)
        return
    else:
        return s
    
def nl():
    print("\n\n")